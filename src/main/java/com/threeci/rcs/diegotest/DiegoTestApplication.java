package com.threeci.rcs.diegotest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Basic main class.
 */
@SpringBootApplication
public class DiegoTestApplication {

        public static void main(String[] args) {
                SpringApplication.run(DiegoTestApplication.class, args);
        }

}
